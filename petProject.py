#Name: Sam LeCompte
#Date: 10/13/15
#Project:petProject

import pet


myPet = pet.Pet("Fido","Dog",5)
print("The name of my pet is", myPet.get_name())
print("The age of my pet is", str(myPet.get_age()))
new_age = input("Enter a new age for your pet: ")
myPet.set_age(new_age)
print("The new age of my pet is", myPet.get_age())
new_type = input("Enter a new type for your pet: ")
myPet.set_animal_type(new_type)
print("Your pet is a", myPet.get_animal_type())
new_name = input("Enter a new name for your pet: ")
myPet.set_name(new_name)
print("The new name of my pet is", myPet.get_name())
