#Name: Sam LeCompte
#Date: 10/5/15 
#Project: Prime

def is_prime(num):

    if num < 2:
        print(num, "is not a valid integer. Please try again")
        return False
    else:
        for x in range(2, num):
            if(num % x == 0):
                return False

    return True

